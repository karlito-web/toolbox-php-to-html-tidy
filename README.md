# Karlito Web

###### Description:  
Toolbox to transform HTML to Formatted Extension 

***
###### Installation:

```cli
composer require karlito-web/toolbox-php-csv
```

***
###### Usage:  

TODO
```php
use KarlitoWeb\Toolbox\PhpToHtmlTidy\FileToHTML\;

$t = new FileToHTML()
$test = FileToHTML::generate($file, null, 'utf8');
TODO
```

TODO

```php
use KarlitoWeb\Toolbox\PhpToHtmlTidy\StringToHTML;

$t = new StringToHTML()
$test = StringToHTML::generate($file, null, 'utf8');
TODO
```

***
##### Contributing:  
Larger projects often have sections on contributing to their project, in which contribution instructions are outlined. Sometimes, this is a separate file. If you have specific contribution preferences, explain them so that other developers know how to best contribute to your work. To learn more about how to help others contribute, check out the guide for setting guidelines for repository contributors.

***
##### Credits:  
Include a section for credits in order to highlight and link to the authors of your project.

***
##### License:  
Finally, include a section for the license of your project. For more information on choosing a license, check out GitHub’s licensing guide!
