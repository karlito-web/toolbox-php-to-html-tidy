<?php

namespace KarlitoWeb\Toolbox\PhpToHtmlTidy;

/**
 * Documentation for Config.
 *
 * @see API https://api.html-tidy.org/tidy/quickref_5.8.0.html
 */
trait Config
{
    /**  @return array */
    private static function getConfig(): array
    {
        return [
            // Document Display Options
            'gnu-emacs' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#gnu-emacs
            'markup' => 'yes',						// https://api.html-tidy.org/tidy/quickref_5.8.0.html#markup
            'mute' => '',							// https://api.html-tidy.org/tidy/quickref_5.8.0.html#
            'mute-id' => 'no',						// https://api.html-tidy.org/tidy/quickref_5.8.0.html#mute-id
            'quiet' => 'no',						// https://api.html-tidy.org/tidy/quickref_5.8.0.html#quiet
            'show-body-only' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#show-body-only
            'show-errors' => 6,						// https://api.html-tidy.org/tidy/quickref_5.8.0.html#show-errors
            'show-filename' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#show-filename
            'show-info' => 'yes',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#show-info
            'show-warnings' => 'yes',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#show-warnings
            // Document In and Out Options
            'add-meta-charset' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#add-meta-charset
            'add-xml-decl' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#add-xml-decl
            'add-xml-space' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#add-xml-space
            'doctype' => 'auto',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#doctype
            'input-xml' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#input-xml
            'output-html' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#output-html
            'output-xhtml' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#output-xhtml
            'output-xml' => 'no',				 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#output-xml
            // File Input-Output Options
            'error-file' => '-',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#error-file
            'keep-time' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#keep-time
            'output-file' => '-',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#output-file
            'write-back' => 'no',				 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#write-back
            // Diagnostics Options
            'accessibility-check' => '0',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#accessibility-check
            'force-output' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#force-output
            'show-meta-change' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#show-meta-change
            'warn-proprietary-attributes' => 'yes', // https://api.html-tidy.org/tidy/quickref_5.8.0.html#warn-proprietary-attributes
            // Encoding Options
            'char-encoding' => 'utf8',			 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#char-encoding  ascii, latin0, latin1, raw, utf8, iso2022, mac, win1252, ibm858, utf16, utf16le, utf16be, big5, shiftjis
            'input-encoding' => 'utf8',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#input-encoding ascii, latin0, latin1, raw, utf8, iso2022, mac, win1252, ibm858, utf16, utf16le, utf16be, big5, shiftjis
            'newline' => 'LF',					 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#newline
            'output-bom' => 'auto',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#output-bom
            'output-encoding' => 'utf8',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#output-encoding	ascii, latin0, latin1, raw, utf8, iso2022, mac, win1252, ibm858, utf16, utf16le, utf16be, big5, shiftjis
            // Cleanup Options
            'bare' => 'no',							// https://api.html-tidy.org/tidy/quickref_5.8.0.html#bare
            'clean' => 'no',						// https://api.html-tidy.org/tidy/quickref_5.8.0.html#clean
            'drop-empty-elements' => 'yes',		 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#drop-empty-elements
            'drop-empty-paras' => 'yes',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#drop-empty-paras
            'drop-proprietary-attributes' => 'no',	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#drop-proprietary-attributes
            'gdoc' => 'no',							// https://api.html-tidy.org/tidy/quickref_5.8.0.html#gdoc
            'logical-emphasis' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#logical-emphasis
            'merge-divs' => 'auto',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#merge-divs
            'merge-spans' => 'auto',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#merge-spans
            'word-2000' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#word-2000
            // Entities Options
            'ascii-chars' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#ascii-chars
            'ncr' => 'yes',						 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#ncr
            'numeric-entities' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#numeric-entities
            'preserve-entities' => 'no',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#preserve-entities
            'quote-ampersand' => 'yes',			 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#quote-ampersand
            'quote-marks' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#quote-marks
            'quote-nbsp' => 'yes',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#quote-nbsp
            // Repair Options
            'alt-text' => '-',					 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#alt-text
            'anchor-as-name' => 'yes',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#anchor-as-name
            'assume-xml-procins' => 'no',		 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#assume-xml-procins
            'coerce-endtags' => 'yes',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#coerce-endtags
            'css-prefix' => 'c',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#css-prefix
            'custom-tags' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#custom
            'enclose-block-text' => 'no',		 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#enclose-block-text
            'enclose-text' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#enclose-text
            'escape-scripts' => 'yes',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#escape-scripts
            'fix-backslash' => 'yes',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#fix-backslash
            'fix-bad-comments' => 'auto',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#fix-bad-comments
            'fix-style-tags' => 'yes',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#fix-style-tags
            'fix-uri' => 'yes',					 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#fix-uri
            'literal-attributes' => 'no',		 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#literal-attributes
            'lower-literals' => 'yes',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#lower-literals
            'repeated-attributes' => 'keep-last',	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#repeated-attributes
            'skip-nested' => 'yes',				 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#skip-nested
            'strict-tags-attributes' => 'no',	 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#strict-tags-attributes
            'uppercase-attributes' => 'no',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#uppercase
            'uppercase-tags' => 'no',			 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#uppercase-tags
            // Transformation Options
            'decorate-inferred-ul' => 'no',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#decorate-inferred-ul
            'escape-cdata' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#escape-cdata
            'hide-comments' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#hide-comments
            'join-classes' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#join-classes
            'join-styles' => 'yes',				 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#join-styles
            'merge-emphasis' => 'yes',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#merge-emphasis
            'replace-color' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#replace-color
            // Teaching Tidy Options
            'new-blocklevel-tags' => '-',	        // Tag Names	-
            'new-empty-tags' => '-',	            // Tag Names	-
            'new-inline-tags' => '-',	            // Tag Names	-
            'new-pre-tags' => '-',	                // Tag Names	-
            // Pretty Print Options
            'break-before-br' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#break-before-br
            'indent' => 'no',						// https://api.html-tidy.org/tidy/quickref_5.8.0.html#indent
            'indent-attributes' => 'no',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#indent-attributes
            'indent-cdata' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#indent-cdata
            'indent-spaces' => 2,					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#indent-spaces
            'indent-with-tabs' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#indent-with-tabs
            'keep-tabs' => 'no',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#keep-tabs
            'omit-optional-tags' => 'no',		 	// https://api.html-tidy.org/tidy/quickref_5.8.0.html#omit-optional-tags
            'priority-attributes' => '-',			// Attributes Names	-
            'punctuation-wrap' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#punctuation-wrap
            'sort-attributes' => 'none',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#sort-attributes
            'tab-size' => 8,						// https://api.html-tidy.org/tidy/quickref_5.8.0.html#tab-size
            'tidy-mark' => 'yes',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#tidy-mark
            'vertical-space' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#vertical-space
            'wrap' => 68,							// https://api.html-tidy.org/tidy/quickref_5.8.0.html#wrap
            'wrap-asp' => 'yes',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#wrap-asp
            'wrap-attributes' => 'no',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#wrap-attributes
            'wrap-jste' => 'yes',					// https://api.html-tidy.org/tidy/quickref_5.8.0.html#wrap-jste
            'wrap-php' => 'no',						// https://api.html-tidy.org/tidy/quickref_5.8.0.html#wrap-php
            'wrap-script-literals' => 'no',			// https://api.html-tidy.org/tidy/quickref_5.8.0.html#wrap-script-literals
            'wrap-sections' => 'yes',				// https://api.html-tidy.org/tidy/quickref_5.8.0.html#wrap-sections
        ];
    }
}
