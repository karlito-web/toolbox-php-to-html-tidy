<?php

namespace KarlitoWeb\Toolbox\PhpToHtmlTidy;

class FileToHTML
{
    use Config;

    /**
     * @param string $filename
     * @param array|string|null $config
     * @param string $encoding // ascii, latin0, latin1, raw, utf8, iso2022, mac, win1252, ibm858, utf16, utf16le, utf16be, big5, shiftjis
     * @param bool $useIncludePath
     * @return string
     */
    public static function generate(
        string $filename,
        null|array|string $config,
        string $encoding = 'utf8',
        bool $useIncludePath = false
    ): string {
        if (is_null($config)) {
            $config = self::getConfig();
        }

        $t = new \tidy();
        $t->parseFile($filename, $config, $encoding, $useIncludePath);
        $t->cleanRepair();

        return $t->html()->value;
    }
}
